start:
	bash ./docker/bin/start.sh
stop:
	bash ./docker/bin/stop.sh
restart:
	make stop ; make start
bash:
	bash ./docker/bin/bash.sh
