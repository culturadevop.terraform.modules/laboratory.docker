FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ARG WORKING_DIR=/workspace

RUN echo 'alias ls="ls --col"' >> /etc/bash.bashrc
RUN apt-get update && apt-get install -y curl gpg gnupg && \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg && \
    NODE_MAJOR=20 && \ 
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list && \
    apt-get update && apt-get install -y \
    git \
    sshpass \
    iputils-ping \
    net-tools \
    ccze \
    nano \
    unzip \
    tar \
    sudo \
    wget \
    libaio1 \
    libpq-dev \
    g++ \
    cron \
    awscli \
    libonig-dev \
    build-essential \
    mariadb-client  \   
    ca-certificates \
    curl \ 
    nodejs &&  \
    # create user
    echo markitos && \
    groupadd -g 1000 markitos && \
    useradd markitos -u 1000 -g 1000 -m -s /bin/bash && echo "markitos:markitos" | chpasswd && adduser markitos sudo && \
    echo "markitos    ALL = (ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    # helm / k8s
    curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null && \
    apt-get install apt-transport-https --yes && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list && \
    curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg && \
    echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list && \
    apt-get update && \
    apt-get install -y helm kubectl && \
    # Clean up
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR ${WORKING_DIR}

USER markitos
RUN mkdir ~/.aws && \
    echo "[xxx]" > ~/.aws/credentials && \
    echo "aws_access_key_id = xxx" >> ~/.aws/credentials && \
    echo "aws_secret_access_key = xxx" >> ~/.aws/credentials && \
    echo "aws_region = eu-west-1" >> ~/.aws/credentials && \
    echo "[profile xxx]" > ~/.aws/config && \
    echo "region = eu-west-1" >> ~/.aws/config

USER root
RUN chown -R markitos:markitos ${WORKING_DIR} && \
    git clone --depth=1 https://github.com/tfutils/tfenv.git /usr/local/tfenv && \
    echo 'export PATH="/usr/local/tfenv/bin:$PATH"' >> /etc/bash.bashrc && \
    export PATH="/usr/local/tfenv/bin:$PATH" && \
    tfenv install 1.7.1 && \
    tfenv use 1.7.1 && \
    mkdir ~/.aws && \
    echo "[xxx]" > ~/.aws/credentials && \
    echo "aws_access_key_id = xxx" >> ~/.aws/credentials && \
    echo "aws_secret_access_key = xxx" >> ~/.aws/credentials && \
    echo "aws_region = eu-west-1" >> ~/.aws/credentials && \
    echo "[profile xxx]" > ~/.aws/config && \
    echo "region = eu-west-1" >> ~/.aws/config && \
    echo 'export PATH=${PATH}:${WORKING_DIR}/node_modules/.bin' >> /etc/bash.bashrc && \
    echo 'export AWS_PROFILE=xxx' >> /etc/bash.bashrc


CMD tail -f  /dev/null